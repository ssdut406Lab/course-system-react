export default {
  extraBabelPlugins: [
    'transform-decorators-legacy',
    'transform-class-properties',
    [
      'import',
      {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: true,
      },
    ],
  ],
  env: {
    development: {
      extraBabelPlugins: [
        "dva-hmr"
      ],
    },
    production: {
      extraBabelPlugins: [],
    },
  },
  hash: false,
  theme: './src/theme.js',
  browserslist: [
    '> 1%',
    'last 4 versions'
  ],
  publicPath: '/',
  proxy: {
    '/api/': {
      target: 'http://172.168.1.247:8080/',
      changeOrigin: true
    }
  },
};
