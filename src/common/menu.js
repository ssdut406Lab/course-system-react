import { isUrl } from '../utils/utils';

const data = [
  {
    name: '管理员',
    path: 'Admins',
    children: [
      {
        name: '首页',
        path: 'ShouYe',
        children: [],
      },
      {
        name: '教务管理',
        path: 'JWGL',
        children: [
          {
            name: '培养计划',
            path: 'PeiyangJH',
            children: [],
          },
          {
            name: '大纲日历',
            path: 'Dagangrili',
            children: [],
          },
          {
            name: '工作量统计',
            path: 'GongzuoliangTJ',
            children: [],
          },
          {
            name: '学生成绩查询',
            path: 'XueshengCJCX',
            children: [],
          }
        ],
      },
      {
        name: '期初设置',
        path: 'QCSZ',
        children: [
          {
            name: '学年设置',
            path: 'XuenianSZ',
            children: [],
          },
          {
            name: '学期设置',
            path: 'XueqiSZ',
            children: [],
          },
          {
            name: '节假日设置',
            path: 'JiejiariSZ',
            children: [],
          },
          {
            name: '周次设置',
            path: 'ZhouciSZ',
            children: [],
          },
          {
            name: '节次设置',
            path: 'JieciSZ',
            children: [],
          },
          {
            name: '教学管理',
            path: 'QCSZ',
            children: [
              {
                name: '课程项目库管理',
                path: 'KCXMKGL',
                children: [
                  {
                    name: '课程题库管理',
                    path: 'KechengtikuGL',
                    children: [],
                  },
                  {
                    name: '课程课件管理',
                    path: 'KechengkejianGL',
                    children: [],
                  },
                  {
                    name: '成绩系数设置',
                    path: 'ChengjixishuSZ',
                    children: [],
                  },
                  {
                    name: '报告模板管理',
                    path: 'BaogaomubanGL',
                    children: [],
                  },
                ],
              },
              {
                name: '本期课程项目管理',
                path: 'BQKCXMGL',
                children: [
                  {
                    name: '课程项目管理',
                    path: 'KechengxinagmuGL',
                    children: [],
                  },
                  {
                    name: '实验项目内容管理',
                    path: 'ShiyanxinagmuNRGL',
                    children: [],
                  },
                  {
                    name: '排课管理',
                    path: 'PaikeGL',
                    children: [],
                  },
                ],
              },
              {
                name: '教学过程管理',
                path: 'JXGCGL',
                children: [
                  {
                    name: '课前预习批阅',
                    path: 'KeqianyuxiPY',
                    children: [],
                  },
                  {
                    name: '实验报告批阅',
                    path: 'ShiyanbaogaoPY',
                    children: [],
                  },
                  {
                    name: '课后作业批阅',
                    path: 'KehouzuoyePY',
                    children: [],
                  },
                ],
              },
              {
                name: '课程成绩管理',
                path: 'KCCJGL',
                children: [
                  {
                    name: '成绩系数管理',
                    path: 'ChengjixishuGL',
                    children: [],
                  },
                  {
                    name: '线下成绩录入',
                    path: 'XianxiachengjiLR',
                    children: [],
                  },
                  {
                    name: '线上成绩汇总',
                    path: 'XianshangchengjiHZ',
                    children: [],
                  },
                  {
                    name: '实验项目成绩汇总',
                    path: 'ShiyanxiangmuCJHZ',
                    children: [],
                  },
                  {
                    name: '课程成绩汇总',
                    path: 'KechengCJHZ',
                    children: [],
                  },
                  {
                    name: '成绩统计分析',
                    path: 'ChengjiTJFX',
                    children: [],
                  },
                ],
              },
              {
                name: '档案归档',
                path: 'DAGD',
                children: [
                  {
                    name: '课程档案管理',
                    path: 'KechengdanganGL',
                    children: [],
                  },
                  {
                    name: '课程结课归档',
                    path: 'KechengJKGD',
                    children: [],
                  },
                ],
              },
              {
                name: '节次设置',
                path: 'JieciSZ',
                children: [],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    name: '学生',
    path: 'Student',
    children: [
      {
        name: '教学管理',
        path: 'JXGL',
        children: [
          {
            name: '课程管理',
            path: 'KCGL',
            children: [
              {
                name: '课后作业',
                path: 'KehouZY',
                children: [],
              },
              {
                name: '教学评价',
                path: 'JiaoxuePJ',
                children: [],
              },
              {
                name: '参加评价',
                path: 'CanjiaPJ',
                children: [],
              },
            ],
          },
        ],
      },
    ],

  }
];

const formatter = (data, parentPath = '/', parentAuthority) => {
  return data.map((item) => {
    let { path } = item;
    if (!isUrl(path)) {
      path = parentPath + item.path;
    }
    const result = {
      ...item,
      path,
      authority: item.authority || parentAuthority,
    };
    if (item.children) {
      result.children = formatter(item.children, `${parentPath}${item.path}/`, item.authority);
    }
    return result;
  });
};

const getMenuData = () => formatter(data);

export const menuData = getMenuData();
