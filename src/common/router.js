import { createElement } from 'react';
import pathToRegexp from 'path-to-regexp';
import dynamic from 'dva/dynamic';
import { menuData } from './menu';

let routerDataCache;

const modelNotExisted = (app, model) => (
  // eslint-disable-next-line
  !app._models.some(({ namespace }) => {
    return namespace === model.substring(model.lastIndexOf('/') + 1);
  })
);

// wrapper of dynamic
const dynamicWrapper = (app, models, component) => {
  // () => require('module')
  // transformed by babel-plugin-dynamic-import-node-sync
  if (component.toString().indexOf('.then(') < 0) {
    models.forEach((model) => {
      if (modelNotExisted(app, model)) {
        // eslint-disable-next-line
        app.model(require(`../models/${model}`).default);
      }
    });
    return (props) => {
      if (!routerDataCache) {
        routerDataCache = getRouterData(app);
      }
      return createElement(component().default, {
        ...props,
        routerData: routerDataCache,
      });
    };
  }
  // () => import('module')
  return dynamic({
    app,
    models: () => models.filter(model => modelNotExisted(app, model)).map(m => import(`../models/${m}.js`)),
    // add routerData prop
    component: () => {
      if (!routerDataCache) {
        routerDataCache = getRouterData(app);
      }
      return component().then((raw) => {
        const Component = raw.default || raw;
        return props => createElement(Component, {
          ...props,
          routerData: routerDataCache,
        });
      });
    },
  });
};

const getFlatMenuData = (menus) => {
  let keys = {};
  menus.forEach((item) => {
    if (item.children) {
      keys[item.path] = { ...item };
      keys = { ...keys, ...getFlatMenuData(item.children) };
    } else {
      keys[item.path] = { ...item };
    }
  });
  return keys;
};

export const getRouterData = (app) => {
  const routerConfig = {
    '/': { component: dynamicWrapper(app, ['global'], () => import('../layout/BasicLayout')), },
    '/Admins/QCSZ/XueqiSZ': { component: dynamicWrapper(app, ['Admins/QCSZ/XueqiSZ'], () => import('../routes/Admins/QCSZ/XueqiSZ')), },
    '/Admins/QCSZ/XuenianSZ': { component: dynamicWrapper(app, ['Admins/QCSZ/XuenianSZ'], () => import('../routes/Admins/QCSZ/XuenianSZ')), },
    '/Admins/QCSZ/JieciSZ': { component: dynamicWrapper(app, ['Admins/QCSZ/JieciSZ'], () => import('../routes/Admins/QCSZ/JieciSZ')), },
    '/Admins/QCSZ/ZhouciSZ': { component: dynamicWrapper(app, ['Admins/QCSZ/ZhouciSZ'], () => import('../routes/Admins/QCSZ/ZhouciSZ')), },
    '/Admins/QCSZ/JiejiariSZ': { component: dynamicWrapper(app, ['Admins/QCSZ/JiejiariSZ'], () => import('../routes/Admins/QCSZ/JiejiariSZ')), },
    '/Admins/JWGL/PeiyangJH': { component: dynamicWrapper(app, ['Admins/JWGL/PeiyangJH'], () => import('../routes/Admins/JWGL/PeiyangJH')), },
    '/Admins/JWGL/Dagangrili': { component: dynamicWrapper(app, ['Admins/JWGL/Dagangrili'], () => import('../routes/Admins/JWGL/Dagangrili')), },
    '/Admins/JWGL/XueshengCJCX': { component: dynamicWrapper(app, ['Admins/JWGL/XueshengCJCX'], () => import('../routes/Admins/JWGL/XueshengCJCX')), },
    '/Admins/JWGL/GongzuoliangTJ': { component: dynamicWrapper(app, ['Admins/JWGL/GongzuoliangTJ'], () => import('../routes/Admins/JWGL/GongzuoliangTJ')), },


    /* '/Admins/JXGL/KCXMKGL/KechengxiangmuGL': { component: dynamicWrapper(app, ['Admins/JXGL/KCXMKGL/KechengxiangmuGL'], () => import('../routes/Admins/JXGL/KCXMKGL/KechengxiangmuGL')), },
     '/Admins/JXGL/KCXMKGL/KechengtikuGL': { component: dynamicWrapper(app, ['Admins/JXGL/KCXMKGL/KechengtikuGL'], () => import('../routes/Admins/JXGL/KCXMKGL/KechengtikuGL')), },
     '/Admins/JXGL/KCXMKGL/KechengkejianGL': { component: dynamicWrapper(app, ['Admins/JXGL/KCXMKGL/KechengkejianGL'], () => import('../routes/Admins/JXGL/KCXMKGL/KechengkejianGL')), },
     '/Admins/JXGL/KCXMKGL/ChengjixishuSZ': { component: dynamicWrapper(app, ['Admins/JXGL/KCXMKGL/ChengjixishuSZ'], () => import('../routes/Admins/JXGL/KCXMKGL/ChengjixishuSZ')), },
     '/Admins/JXGL/KCXMKGL/BaogaomubanGL': { component: dynamicWrapper(app, ['Admins/JXGL/KCXMKGL/BaogaomubanGL'], () => import('../routes/Admins/JXGL/KCXMKGL/BaogaomubanGL')), },
     '/Admins/JXGL/BQKCXMGL/KechengxiangmuGL': { component: dynamicWrapper(app, ['Admins/JXGL/BQKCXMGL/KechengxiangmuGL'], () => import('../routes/Admins/JXGL/BQKCXMGL/KechengxiangmuGL')), },
     '/Admins/JXGL/BQKCXMGL/ShiyanxiangmuNRGL': { component: dynamicWrapper(app, ['Admins/JXGL/BQKCXMGL/ShiyanxiangmuNRGL'], () => import('../routes/Admins/JXGL/BQKCXMGL/ShiyanxiangmuNRGL')), },
     '/Admins/JXGL/BQKCXMGL/PaikeGL': { component: dynamicWrapper(app, ['Admins/JXGL/BQKCXMGL/PaikeGL'], () => import('../routes/Admins/JXGL/BQKCXMGL/PaikeGL')), },
     '/Admins/JXGL/JXGCGL/KeqianyuxiPY': { component: dynamicWrapper(app, ['Admins/JXGL/JXGCGL/KeqianyuxiPY'], () => import('../routes/Admins/JXGL/JXGCGL/KeqianyuxiPY')), },
     '/Admins/JXGL/JXGCGL/ShiyanbaogaoPY': { component: dynamicWrapper(app, ['Admins/JXGL/JXGCGL/ShiyanbaogaoPY'], () => import('../routes/Admins/JXGL/JXGCGL/ShiyanbaogaoPY')), },
     '/Admins/JXGL/JXGCGL/KehouzuoyePY': { component: dynamicWrapper(app, ['Admins/JXGL/JXGCGL/KehouzuoyePY'], () => import('../routes/Admins/JXGL/JXGCGL/KehouzuoyePY')), },
     '/Admins/JXGL/KCCJGL/ChengjixishuSZ': { component: dynamicWrapper(app, ['Admins/JXGL/KCCJGL/ChengjixishuSZ'], () => import('../routes/Admins/JXGL/KCCJGL/ChengjixishuSZ')), },
     '/Admins/JXGL/KCCJGL/XianxiachengjiLR': { component: dynamicWrapper(app, ['Admins/JXGL/KCCJGL/XianxiachengjiLR'], () => import('../routes/Admins/JXGL/KCCJGL/XianxiachengjiLR')), },
     '/Admins/JXGL/KCCJGL/XianshangchangjiHZ': { component: dynamicWrapper(app, ['Admins/JXGL/KCCJGL/XianshangchangjiHZ'], () => import('../routes/Admins/JXGL/KCCJGL/XianshangchangjiHZ')), },
     '/Admins/JXGL/KCCJGL/ShiyanxiangmuchegnjiHZ': { component: dynamicWrapper(app, ['Admins/JXGL/KCCJGL/ShiyanxiangmuchegnjiHZ'], () => import('../routes/Admins/JXGL/KCCJGL/ShiyanxiangmuchegnjiHZ')), },
     '/Admins/JXGL/KCCJGL/KechengCJHZ': { component: dynamicWrapper(app, ['Admins/JXGL/KCCJGL/KechengCJHZ'], () => import('../routes/Admins/JXGL/KCCJGL/KechengCJHZ')), },
     '/Admins/JXGL/KCCJGL/ChengjiTJFX': { component: dynamicWrapper(app, ['Admins/JXGL/KCCJGL/ChengjiTJFX'], () => import('../routes/Admins/JXGL/KCCJGL/ChengjiTJFX')), },
     '/Admins/JXGL/DAYGD/KechengdanganGL': { component: dynamicWrapper(app, ['Admins/JXGL/DAYGD/KechengdanganGL'], () => import('../routes/Admins/JXGL/DAYGD/KechengdanganGL')), },
     '/Admins/JXGL/DAYGD/KechengJKGD': { component: dynamicWrapper(app, ['Admins/JXGL/DAYGD/KechengJKGD'], () => import('../routes/Admins/JXGL/DAYGD/KechengJKGD')), },
 */


    '/Admins/ShouYe': { component: dynamicWrapper(app, ['Admins/ShouYe'], () => import('../routes/Admins/ShouYe')), },
    '/Student/JXGL/KCGL/KehouZY': { component: dynamicWrapper(app, ['Student/JXGL/KCGL/KehouZY'], () => import('../routes/Students/JXGL/KCGL/KehouZY')), },
    '/Student/JXGL/KCGL/JiaoxuePJ': { component: dynamicWrapper(app, ['Student/JXGL/KCGL/JiaoxuePJ'], () => import('../routes/Students/JXGL/KCGL/JiaoxuePJ')), },
    '/Student/JXGL/KCGL/CanjiaPJ': { component: dynamicWrapper(app, ['Student/JXGL/KCGL/CanjiaPJ'], () => import('../routes/Students/JXGL/KCGL/CanjiaPJ')), },
  };
  // Get name from ./menu.js or just set it in the router data.
  const data = getFlatMenuData(menuData);

  // Route configuration data
  // eg. {name,authority ...routerConfig }
  const routerData = {};
  // The route matches the menu
  Object.keys(routerConfig).forEach((path) => {
    // Regular match item name
    // eg.  router /user/:id === /user/chen
    const pathRegexp = pathToRegexp(path);
    const menuKey = Object.keys(data).find(key => pathRegexp.test(`${key}`));
    let menuItem = {};
    // If menuKey is not empty
    if (menuKey) {
      menuItem = data[menuKey];
    }
    let router = routerConfig[path];
    // If you need to configure complex parameter routing,
    // https://github.com/ant-design/ant-design-pro-site/blob/master/docs/router-and-nav.md#%E5%B8%A6%E5%8F%82%E6%95%B0%E7%9A%84%E8%B7%AF%E7%94%B1%E8%8F%9C%E5%8D%95
    // eg . /list/:type/user/info/:id
    router = {
      ...router,
      name: router.name || menuItem.name,
      authority: router.authority || menuItem.authority,
    };
    routerData[path] = router;
  });
  return routerData;
};
