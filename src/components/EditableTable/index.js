import React, { Component } from 'react';
import { Divider, Input, Popconfirm, Table } from 'antd';
import PropTypes from 'prop-types';

export default class EditableTable extends Component {

  handleChange = (value, key, column) => {
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target[column] = value;
      this.setState({ dataSource: newData });
    }
  };

  renderColumns = (text, record, column) => (
    <div>
      {
        record.editable ?
          <Input
            style={{ margin: '-5px 0' }}
            value={text}
            onChange={e => this.handleChange(e.target.value, record.key, column)}
          />
          : text
      }
    </div>
  );

  cancel = (record) => {
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => record.key === item.key)[0];
    if (target) {
      target.editable = false;
      this.setState({ dataSource: newData });
    }
  };

  edit = (record) => {
    const newData = [...this.state.dataSource];
    const target = newData.filter(item => record.key === item.key)[0];
    if (target) {
      target.editable = true;
      this.setState({ dataSource: newData });
    }
  };

  operation = {
    title: '操作',
    dataIndex: 'operation',
    width: '25%',
    render: (text, record) => {
      const { editable } = record;
      if (editable) {
        return (
          <div>
            <span>
              <a onClick={() => this.props.save(record)}>保存</a>
              <Divider type="vertical"/>
              <Popconfirm title="确定取消？" onConfirm={() => this.cancel(record)}>
                <a>取消</a>
              </Popconfirm>
            </span>
          </div>
        )
      } else {
        return (
          <div>
             <span>
               <a onClick={() => this.edit(record)}>编辑</a>
               <Divider type="vertical"/>
               <a href="#">删除</a>
               <Divider type="vertical"/>
               <a href="#">详情</a>
             </span>
          </div>
        )
      }
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      dataSource: props.dataSource.map((item, i) => ({ ...item, key: `${i}` })),
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      dataSource: nextProps.dataSource.map((item, i) => ({ ...item, key: `${i}` })),
    });
  }

  render() {
    const { columns } = this.props;
    const realColumns = columns.map((item) => ({
      ...item,
      render: (text, record) => this.renderColumns(text, record, item.dataIndex),
    }));

    realColumns.push(this.operation);

    return (
      <div>
        <Table
          columns={realColumns}
          dataSource={this.state.dataSource}
          pagination={{ showSizeChanger: true }}
        />
      </div>
    );
  }
}

EditableTable.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.object),
  dataSource: PropTypes.arrayOf(PropTypes.object),
  save: PropTypes.func.isRequired,
};

EditableTable.defaultProps = {
  columns: [],
  dataSource: [],
};
