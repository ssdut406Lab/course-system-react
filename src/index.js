import '@babel/polyfill';
import 'url-polyfill';
import 'whatwg-fetch'
import dva from 'dva';
import createHistory from 'history/createBrowserHistory';
import './index.less';
import router from './router';
import createLoading from 'dva-loading';
import moment from 'moment';

moment.locale('zh-cn');

// 1. Initialize
const app = dva({
  history: createHistory(),
});

// 2. Plugins
app.use(createLoading());

// 3. Model
// app.model(require('./models/example').default);

// 4. Router
app.router(router);

// 5. Start
app.start('#root');
