import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import styles from './BasicLayout.less';
import PropTypes from 'prop-types';
import { menuData } from '../common/menu';
import Exception from "ant-design-pro/lib/Exception/index";
import { Link, Redirect, Route, Switch } from 'dva/router';
import { getRoutes } from "../utils/utils";
import { Scrollbars } from 'react-custom-scrollbars';

const { Footer, Sider, Content } = Layout;
const { SubMenu } = Menu;

export default class BasicLayout extends Component {

  getFlatMenuKeys = (menus) => {
    let keys = [];
    menus.forEach((item) => {
        if (item.children) {
          keys = keys.concat(this.getFlatMenuKeys(item.children));
        }
        keys.push(item.path);
      }
    );
    return keys;
  };

  constructor(props) {
    super(props);
  }

  render() {

    const { match, routerData, username, location: { pathname } } = this.props;

    const menu = menuData.filter(menu => pathname.startsWith(`${menu.path}/`))[0];
    if (menu === undefined) {
      return (
        <Exception/>
      );
    }

    const item = menu.children.filter(item => pathname.startsWith(`${item.path}/`))[0];
    if (item === undefined) {
      return (
        <Exception/>
      );
    }

    return (
      <Layout className={styles.layout}>
        <Scrollbars style={{ height: '100vh' }} autoHide>
          <div className={styles.title}>实验教学管理信息系统</div>
          <nav className={styles.nav}>
            <Menu mode="horizontal" theme="dark" className={styles.navMenu} selectedKeys={[]}>
              {
                menu.children.map(item => {
                  if (item.children.length === 0) {
                    return (
                      <Menu.Item key={item.path}>
                        <Link to={item.path}>{item.name}</Link>
                      </Menu.Item>
                    )
                  } else {
                    return (
                      <SubMenu title={item.name} key={item.path}>
                        {
                          item.children.map(subItem => (
                            <Menu.Item key={subItem.path}>
                              <Link to={subItem.path}>{subItem.name}</Link>
                            </Menu.Item>
                          ))
                        }
                      </SubMenu>
                    )
                  }
                })
              }
            </Menu>
            <div className={styles.navInfo}>
              <div>
                <span>{username}</span>
                &nbsp;
                <a href="" className={styles.logout}>注销</a>
              </div>
            </div>
          </nav>
          <Layout style={{ height: 'calc(100vh - 50px)' }}>
            <Sider className={styles.sider}>
              <Scrollbars className={styles.scrollbar} autoHide>
                <Menu mode="inline" className={styles.siderMenu} selectedKeys={[pathname]}>
                  {
                    item.children.map(it => {
                      if (it.children.length === 0) {
                        return (
                          <Menu.Item key={it.path}>
                            <Link to={it.path}>{it.name}</Link>
                          </Menu.Item>
                        )
                      } else {
                        return (
                          <SubMenu title={it.name} key={it.path}>
                            {
                              it.children.map(subIt => (
                                <Menu.Item key={subIt.path}>
                                  <Link to={subIt.path}>{subIt.name}</Link>
                                </Menu.Item>
                              ))
                            }
                          </SubMenu>
                        )
                      }
                    })
                  }
                </Menu>
              </Scrollbars>
            </Sider>
            <Content className={styles.container}>
              <Scrollbars className={styles.scrollbar} autoHide>
                <Content className={styles.content}>
                  <Switch>
                    {
                      getRoutes(match.path, routerData).map(item => (
                        <Route component={item.component} key={item.key} path={item.path}/>
                      ))
                    }
                    <Route render={Exception}/>
                  </Switch>
                </Content>
                <Footer className={styles.footer}>
                  <div>
                    版权所有&copy;大连理工大学软件学院实验教学示范中心
                  </div>
                  <div>
                    地址: 大连经济技术开发区图强街321号&nbsp;Email: ssdut@dlut.edu.cn&nbsp;电话: 0411-62274467&nbsp;技术支持:XXX
                  </div>
                </Footer>
              </Scrollbars>
            </Content>
          </Layout>
        </Scrollbars>
      </Layout>
    );
  }
}

BasicLayout.propTypes = {
  username: PropTypes.string,
};

BasicLayout.defaultProps = {
  username: '管理员',
};
