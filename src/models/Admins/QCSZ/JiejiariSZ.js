import modelExtend from 'dva-model-extend';
import common from '../../common';

export default modelExtend(common, {

  namespace: 'JiejiariSZ',

  state: {},

  subscriptions: {},

  effects: {},

  reducers: {},

});
