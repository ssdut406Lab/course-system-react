import modelExtend from 'dva-model-extend';
import common from '../../common';
import { query, request } from "../../../services/request";

export default modelExtend(common, {

  namespace: 'XuenianSZ',

  state: {
    data: [
      { xnbh: '', xnmc: '', xnpxh: '', xnkssj: '', xnjssj: '' }
    ],
  },

  subscriptions: {},

  effects: {
    * load({ payload }, { call, put, select }) {
      const json = yield call(query, '/api/admins/qcsz/xueniansz');
      yield put({
        type: 'save',
        payload: { data: json.data },
      });
    },
    * create({ payload }, { call, put }) {
      const json = yield call(request, '', payload);
      yield put({
        type: 'save',
        payload: { data: payload.data },
      })

    }
  },

  reducers: {
    save(state, { payload }) {
      return { ...state, ...payload };
    },
  },

});
