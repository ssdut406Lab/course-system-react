import modelExtend from 'dva-model-extend';
import common from '../common';

export default modelExtend(common, {

  namespace: 'ShouYe',

  state: {},

  subscriptions: {},

  effects: {},

  reducers: {},

});
