import React from 'react';
import { Route, routerRedux, Switch } from 'dva/router';
import dynamic from 'dva/dynamic';
import { getRouterData } from './common/router';
import { Spin } from 'antd';
import styles from './index.less';

dynamic.setDefaultLoadingComponent(() => {
  return <Spin size="large" className={styles.globalSpin}/>;
});

const { ConnectedRouter } = routerRedux;

const RouterConfig = ({ history, app }) => {

  const routerData = getRouterData(app);
  const BasicLayout = routerData["/"].component;

  return (
    <ConnectedRouter history={history}>
      <Switch>
        <Route path="/" render={props => {
          return (<BasicLayout{...props}/>)
        }}/>
      </Switch>
    </ConnectedRouter>
  );
};

export default RouterConfig;
