import React, { Component, Fragment } from 'react';
import { Button, Dropdown, Icon, Menu, Tabs } from 'antd';
import style from './index.less';
import GongzuolinagTJTable from './table';

const { TabPane } = Tabs;
const onClick = ({ key }) => {

};

const menu = (
  <Menu onClick={onClick}>
    <Menu.Item key="1">1st menu item</Menu.Item>
    <Menu.Item key="2">2nd memu item</Menu.Item>
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);

export default class GongzuolinagTJ extends Component {
  render() {
    return (
      <Fragment>
        <Tabs type="card" className={style.gongzuolinagTJInfo}>
          <TabPane tab="工作量统计" key="1">
            <div className={style.gongzuolinagTJ}>
              <div className={style.topSearch}>
                <div>
                  <span>学期：</span>
                  <Dropdown overlay={menu}>
                    <a className="ant-dropdown-link" href="#">
                      2017-2018秋 <Icon type="down"/>
                    </a>
                  </Dropdown>
                </div>
                <div>
                  <span>院系：</span>
                  <Dropdown overlay={menu}>
                    <a className="ant-dropdown-link" href="#">
                      软件学院 <Icon type="down"/>
                    </a>
                  </Dropdown>
                </div>
                <Icon type="search" className={style.iconSearch}/>
              </div>
              <div className={style.operation}>
                <Button htmlType={"button"}>刷新</Button>
                <Button htmlType={"button"}>导出</Button>
                <Button htmlType={"button"}>打印</Button>
              </div>
              <GongzuolinagTJTable/>
            </div>
          </TabPane>
          <TabPane tab="Tab 2" key="2">Content of Tab Pane 2</TabPane>
          <TabPane tab="Tab 3" key="3">Content of Tab Pane 3</TabPane>
        </Tabs>
      </Fragment>
    );
  }
}
