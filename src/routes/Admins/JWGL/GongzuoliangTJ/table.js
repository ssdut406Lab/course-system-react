import React, { Component } from 'react';
import { Divider, Input, Popconfirm, Table } from 'antd';

const data = [];
for (let i = 0; i < 120; i++) {
  data.push({
    key: i,
    semeNo: `${i}`,
    semeName: `学年${i}`,
    order: `排序 ${i}`,
    semeStart: '',
    semeEnd: '',
  });
}

const EditableCell = ({ editable, value, onChange }) => (
  <div>
    {editable
      ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)}/>
      : value
    }
  </div>
);

const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
  },
  getCheckboxProps: record => ({
    disabled: record.name === 'Disabled User', // Column configuration not to be checked
    name: record.name,
  }),
};

export default class GongzuolinagTJTable extends Component {
  constructor(props) {
    super(props);
    this.columns = [{
      title: '教师姓名',
      dataIndex: 'teachName',
      width: '10%',
      render: (text, record) => this.renderColumns(text, record, 'teachName'),
    }, {
      title: '教师编号',
      dataIndex: 'teachNo',
      width: '10%',
      render: (text, record) => this.renderColumns(text, record, 'teachNo'),
    }, {
      title: '课程编号',
      dataIndex: 'courseNo',
      width: '10%',
      render: (text, record) => this.renderColumns(text, record, 'courseNo'),
    }, {
      title: '课程名称',
      dataIndex: 'courseName',
      render: (text, record) => this.renderColumns(text, record, 'courseName'),
    }, {
      title: '总学时（主+辅）',
      dataIndex: 'totleTime',
      render: (text, record) => this.renderColumns(text, record, 'totleTime'),
    },
      {
        title: '总人时数',
        dataIndex: 'allTime',
        render: (text, record) => this.renderColumns(text, record, 'allTime'),
      }, {
        title: '操作',
        dataIndex: 'operation',
        width: '25%',
        render: (text, record) => {
          const { editable } = record;
          return (
            <div className="editable-row-operations">
              {
                editable ?
                  <span>
                  <a onClick={() => this.save(record.key)}>保存</a>
                  <Divider type="vertical"/>
                  <Popconfirm title="确定取消？" onConfirm={() => this.cancel(record.key)}>
                    <a>取消</a>
                  </Popconfirm>
                </span>
                  : <span>
                    <a onClick={() => this.edit(record.key)}>修改</a>
                    <Divider type="vertical"/>
                    <a href="#" className="ant-dropdown-link">详情</a>
                  </span>
              }
            </div>
          );
        },
      }];
    this.state = { data };
    this.cacheData = data.map(item => ({ ...item }));
  }

  renderColumns(text, record, column) {
    return (
      <EditableCell
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value, record.key, column)}
      />
    );
  }

  handleChange(value, key, column) {
    const newData = [...this.state.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target[column] = value;
      this.setState({ data: newData });
    }
  }

  edit(key) {
    const newData = [...this.state.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target.editable = true;
      this.setState({ data: newData });
    }
  }

  save(key) {
    const newData = [...this.state.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      delete target.editable;
      this.setState({ data: newData });
      this.cacheData = newData.map(item => ({ ...item }));
    }
  }

  cancel(key) {
    const newData = [...this.state.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
      delete target.editable;
      this.setState({ data: newData });
    }
  }

  render() {
    return <Table bordered rowSelection={rowSelection} dataSource={this.state.data} columns={this.columns}
                  pagination={{ showSizeChanger: true }}/>;
  }
}

