import React, { Component, Fragment } from 'react';
import { Button, Dropdown, Icon, Menu, Tabs } from 'antd';
import style from './index.less';
import XueshengCJCXTable from './table';

const { TabPane } = Tabs;
const onClick = ({ key }) => {

};

const menu = (
  <Menu onClick={onClick}>
    <Menu.Item key="1">1st menu item</Menu.Item>
    <Menu.Item key="2">2nd memu item</Menu.Item>
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);

export default class XueshengCJCX extends Component {
  render() {
    return (
      <Fragment>
        <Tabs type="card" className={style.xueshengCJCXInfo}>
          <TabPane tab="学生成绩查询" key="1">
            <div className={style.xueshengCJCX}>
              <div className={style.topSearch}>
                <div>
                  <span>学期：</span>
                  <Dropdown overlay={menu}>
                    <a className="ant-dropdown-link" href="#">
                      2017-2018秋 <Icon type="down"/>
                    </a>
                  </Dropdown>
                </div>
                <div>
                  <span>学号：</span>
                  <Dropdown overlay={menu}>
                    <a className="ant-dropdown-link" href="#">
                      201592190 <Icon type="down"/>
                    </a>
                  </Dropdown>
                </div>
                <div>
                  <span>课程：</span>
                  <Dropdown overlay={menu}>
                    <a className="ant-dropdown-link" href="#">
                      数模电 <Icon type="down"/>
                    </a>
                  </Dropdown>
                </div>
                <Icon type="search" className={style.iconSearch}/>
              </div>
              <div className={style.operation}>
                <Button htmlType={"button"}>刷新</Button>
                <Button htmlType={"button"}>下载</Button>
                <Button htmlType={"button"}>打印</Button>
              </div>
              <XueshengCJCXTable/>
            </div>
          </TabPane>
          <TabPane tab="Tab 2" key="2">Content of Tab Pane 2</TabPane>
          <TabPane tab="Tab 3" key="3">Content of Tab Pane 3</TabPane>
        </Tabs>
      </Fragment>
    );
  }
}
