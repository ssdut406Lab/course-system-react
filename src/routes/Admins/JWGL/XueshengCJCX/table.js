import React, { Component } from 'react';
import { Input, Table } from 'antd';

const data = [];
for (let i = 0; i < 120; i++) {
  data.push({
    key: i,
    semeNo: `${i}`,
    semeName: `学年${i}`,
    order: `排序 ${i}`,
    semeStart: '',
    semeEnd: '',
  });
}

const EditableCell = ({ editable, value, onChange }) => (
  <div>
    {editable
      ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)}/>
      : value
    }
  </div>
);
const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
  },
  getCheckboxProps: record => ({
    disabled: record.name === 'Disabled User', // Column configuration not to be checked
    name: record.name,
  }),
};

export default class XueshengCJCXTable extends Component {
  constructor(props) {
    super(props);
    this.columns = [{
      title: '课程名称',
      dataIndex: 'className',
      width: '10%',
      render: (text, record) => this.renderColumns(text, record, 'className'),
    }, {
      title: '课程编号',
      dataIndex: 'classNo',
      width: '10%',
      render: (text, record) => this.renderColumns(text, record, 'classNo'),
    }, {
      title: '课序号',
      dataIndex: 'order',
      width: '10%',
      render: (text, record) => this.renderColumns(text, record, 'order'),
    }, {
      title: '主讲教师',
      dataIndex: 'mainTeach',
      render: (text, record) => this.renderColumns(text, record, 'semeStart'),
    }, {
      title: '总成绩',
      dataIndex: 'teachOutline',
      render: (text, record) => this.renderColumns(text, record, 'teachOutline'),
    }, {
      title: '操作',
      dataIndex: 'operation',
      width: '25%',
      render: (text, record) => {
        const { editable } = record;
        return (
          <span>
            <a href="#" className="ant-dropdown-link">详情</a>
          </span>
        );
      },
    }];
    this.state = { data };
    this.cacheData = data.map(item => ({ ...item }));
  }

  renderColumns(text, record, column) {
    return (
      <EditableCell
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value, record.key, column)}
      />
    );
  }

  handleChange(value, key, column) {
    const newData = [...this.state.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target[column] = value;
      this.setState({ data: newData });
    }
  }

  save(key) {
    const newData = [...this.state.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      delete target.editable;
      this.setState({ data: newData });
      this.cacheData = newData.map(item => ({ ...item }));
    }
  }

  render() {
    return <Table bordered rowSelection={rowSelection} dataSource={this.state.data} columns={this.columns}
                  pagination={{ showSizeChanger: true }}/>;
  }
}

