import React, { Component, Fragment } from 'react';
import { Button, Divider, Dropdown, Icon, Input, Menu, Popconfirm, Table, Tabs } from 'antd';
import style from './index.less';
import { connect } from 'dva';

const TabPane = Tabs.TabPane;
const onClick = ({ key }) => {

};

const menu = (
  <Menu onClick={onClick}>
    <Menu.Item key="1">1st menu item</Menu.Item>
    <Menu.Item key="2">2nd memu item</Menu.Item>
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);

const EditableCell = ({ editable, value, onChange }) => (
  <div>
    {editable
      ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)}/>
      : value
    }
  </div>
);


@connect(({ JieciSZ }) => ({ data: JieciSZ.data }))
export default class JieciSZ extends Component {
  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    loaded: false,
  };
  columns = [{
    title: '节次编号',
    dataIndex: 'hdbh',
    width: '10%',
    render: (text, record) => this.renderColumns(text, record, 'hdbh'),
  }, {
    title: '节次名称',
    dataIndex: 'hdmc',
    width: '10%',
    render: (text, record) => this.renderColumns(text, record, 'hdmc'),
  }, {
    title: '大节名称',
    dataIndex: 'djmc',
    width: '10%',
    render: (text, record) => this.renderColumns(text, record, 'djmc'),
  }, {
    title: '开始时间',
    dataIndex: 'hdkssj',
    render: (text, record) => this.renderColumns(text, record, 'hdkssj'),
  }, {
    title: '结束时间',
    dataIndex: 'hdjssj',
    render: (text, record) => this.renderColumns(text, record, 'hdjssj'),
  }, {
    title: '操作',
    key: 'action',
    width: '25%',
    render: (text, record) => {
      const { editable } = record;
      return (
        <div className="editable-row-operations">
          {
            editable ?
              <span>
                  <a onClick={() => this.save(record.key)}>保存</a>
                  <Divider type="vertical"/>
                  <Popconfirm title="确定取消？" onConfirm={() => this.cancel(record.key)}>
                    <a>取消</a>
                  </Popconfirm>
                </span>
              : <span>
                    <a onClick={() => this.edit(record.key)}>编辑</a>
                    <Divider type="vertical"/>
                    <a href="#">删除</a>
                  </span>
          }
        </div>
      );
    }
  }];
  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  };

  load = () => {
    this.props.dispatch({
      type: 'JieciSZ/load',
    });
  };

  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
    this.props.dispatch({
      type: 'XuenianSZ/load',
      payload: {
        callback: () => {
          console.log(this);
          this.setState({ data: this.props.data });
          this.cacheData = this.props.data.map(item => ({ ...item }));
        }
      }
    });
  }

  renderColumns(text, record, column) {
    return (
      <EditableCell
        editable={record.editable}
        value={text}
        onChange={value => this.handleChange(value, record.key, column)}
      />
    );
  }

  handleChange(value, key, column) {
    const newData = [...this.props.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target[column] = value;
      this.setState({ data: newData });
    }
  }

  edit(key) {
    const newData = [...this.props.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      target.editable = true;
      this.setState({ data: newData });
    }
  }

  save(key) {
    const newData = [...this.props.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      delete target.editable;
      this.setState({ data: newData });
      this.cacheData = newData.map(item => ({ ...item }));
    }
  }

  cancel(key) {
    const newData = [...this.props.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
      delete target.editable;
      this.setState({ data: newData });
    }
  }

  render() {
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    if (!this.state.loaded) {
      this.load();
    }
    return (
      <Fragment>
        <Tabs type="card" className={style.jieciSZInfo}>
          <TabPane tab="节次设置" key="1">
            <div className={style.jieciSZ}>
              <div className={style.topSearch}>
                <div>
                  <span>单位：</span>
                  <Dropdown overlay={menu}>
                    <a className="ant-dropdown-link" href="#">
                      大连理工大学 <Icon type="down"/>
                    </a>
                  </Dropdown>
                </div>
                <div>
                  <span>时区：</span>
                  <Dropdown overlay={menu}>
                    <a className="ant-dropdown-link" href="#">
                      北京-堂时 <Icon type="down"/>
                    </a>
                  </Dropdown>
                </div>
                <Icon type="search" className={style.iconSearch}/>
              </div>
              <div className={style.operation}>
                <Button htmlType={"button"}>添加</Button>
                <Button htmlType={"button"}>删除</Button>
                <Button htmlType={"button"}>刷新</Button>
                <Button htmlType={"button"}>保存</Button>
              </div>
              <div>
                <div style={{ marginBottom: 16 }}>
                  <span style={{ marginLeft: 8 }}> </span>
                </div>
                <Table rowSelection={rowSelection} columns={this.columns} dataSource={this.props.data}
                       pagination={{ showSizeChanger: true }}/>
              </div>
            </div>
          </TabPane>
          <TabPane tab="Tab 2" key="2">Content of Tab Pane 2</TabPane>
          <TabPane tab="Tab 3" key="3">Content of Tab Pane 3</TabPane>
        </Tabs>
      </Fragment>
    );
  }
}
