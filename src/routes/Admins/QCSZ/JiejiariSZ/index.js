import React, { Component, Fragment } from 'react';
import { Badge, Button, Calendar, Dropdown, Icon, Menu, Tabs } from 'antd';
import style from './index.less';

const { TabPane } = Tabs;
const onClick = ({ key }) => {

};

const menu = (
  <Menu onClick={onClick}>
    <Menu.Item key="1">1st menu item</Menu.Item>
    <Menu.Item key="2">2nd memu item</Menu.Item>
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);

const getListData = (value) => {
  switch (value.date()) {
    case 8:
      return [
        { type: 'warning', content: '报道' },
      ];
    case 10:
      return [
        { type: 'success', content: '放假' },
      ];
    case 15:
      return [
        { type: 'error', content: '考试' },
      ];
    default:
      return [];
  }
};

const dateCellRender = (value) => {
  const listData = getListData(value);
  return (
    <ul className="events">
      {
        listData.map(item => (
          <li key={item.content}>
            <Badge status={item.type} text={item.content}/>
          </li>
        ))
      }
    </ul>
  );
};

const getMonthData = (value) => {
  if (value.month() === 8) {
    return 1394;
  }
};

const monthCellRender = (value) => {
  const num = getMonthData(value);
  return num ? (
    <div className="notes-month">
      <section>{num}</section>
      <span>Backlog number</span>
    </div>
  ) : null;
};

export default class JiejiariSZ extends Component {
  render() {
    return (
      <Fragment>
        <Tabs type="card" className={style.jiejiariSZInfo}>
          <TabPane tab="节假日设置" key="1">
            <div className={style.jiejiariSZ}>
              <div className={style.topSearch}>
                <div>
                  <span>单位：</span>
                  <Dropdown overlay={menu}>
                    <a className="ant-dropdown-link" href="#">
                      大连理工大学 <Icon type="down"/>
                    </a>
                  </Dropdown>
                </div>
                <Icon type="search" className={style.iconSearch}/>
              </div>
              <div className={style.operation}>
                <Button htmlType={"button"}>添加</Button>
                <Button htmlType={"button"}>删除</Button>
                <Button htmlType={"button"}>刷新</Button>
                <Button htmlType={"button"}>保存</Button>
              </div>
              <Calendar dateCellRender={dateCellRender} monthCellRender={monthCellRender} className={style.calender}/>
            </div>
          </TabPane>
          <TabPane tab="Tab 2" key="2">Content of Tab Pane 2</TabPane>
          <TabPane tab="Tab 3" key="3">Content of Tab Pane 3</TabPane>
        </Tabs>
      </Fragment>
    );
  }
}
