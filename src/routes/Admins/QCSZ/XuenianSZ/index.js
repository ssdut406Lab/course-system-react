import React, { Component, Fragment } from 'react';
import { Button, Dropdown, Icon, Menu, Tabs } from 'antd';
import style from './index.less';
import { connect } from "dva";
import EditableTable from "../../../../components/EditableTable";

const { TabPane } = Tabs;
const onClick = ({ key }) => {

};

const menu = (
  <Menu onClick={onClick}>
    <Menu.Item key="1">1st menu item</Menu.Item>
    <Menu.Item key="2">2nd memu item</Menu.Item>
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);

@connect(({ XuenianSZ }) => ({ data: XuenianSZ.data }))
export default class XuenianSZ extends Component {
  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
  };

  columns = [
    {
      title: '学年编号',
      dataIndex: 'xnbh',
      width: '10%',
    },
    {
      title: '学年名称',
      dataIndex: 'xnmc',
      width: '10%',
    },
    {
      title: '排序号',
      dataIndex: 'xnpxh',
      width: '10%',
    },
    {
      title: '学年开始时间',
      dataIndex: 'xnkssj',
    },
    {
      title: '学年结束时间',
      dataIndex: 'xnjssj',
    },
  ];

  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  };

  load = () => {
    this.props.dispatch({
      type: 'XuenianSZ/load',
    });
  };

  constructor(props) {
    super(props);
    this.props.dispatch({
      type: 'XuenianSZ/load',
    });
  }

  save(key) {
    const newData = [...this.props.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      delete target.editable;
      this.setState({ data: newData });
      this.cacheData = newData.map(item => ({ ...item }));
    }
  }

  render() {
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    if (!this.state.loaded) {
      this.load();
    }
    return (
      <Fragment>
        <Tabs type="card" className={style.xuenianSZInfo}>
          <TabPane tab="学年设置" key="1">
            <div className={style.xuenianSZ}>
              <div className={style.topSearch}>
                <div>
                  <span>单位：</span>
                  <Dropdown overlay={menu}>
                    <a className="ant-dropdown-link" href="#">
                      大连理工大学 <Icon type="down"/>
                    </a>
                  </Dropdown>
                </div>
                <div>
                  <span>学年：</span>
                  <Dropdown overlay={menu}>
                    <a className="ant-dropdown-link" href="#">
                      2017-2018 <Icon type="down"/>
                    </a>
                  </Dropdown>
                </div>
                <Icon type="search" className={style.iconSearch}/>
              </div>
              <div className={style.operation}>
                <Button htmlType={"button"}>增加</Button>
                <Button htmlType={"button"}>删除</Button>
                <Button htmlType={"button"}>刷新</Button>
                <Button htmlType={"button"}>保存</Button>
              </div>
              <div>
                <div style={{ marginBottom: 16 }}>
                  <span style={{ marginLeft: 8 }}> </span>
                </div>
                <EditableTable
                  dataSource={this.props.data}
                  columns={this.columns}
                  save={this.save.bind(this)}
                  rowSelection={rowSelection}
                />
              </div>
            </div>
          </TabPane>
          <TabPane tab="Tab 2" key="2">Content of Tab Pane 2</TabPane>
          <TabPane tab="Tab 3" key="3">Content of Tab Pane 3</TabPane>
        </Tabs>
      </Fragment>
    );
  }
}
