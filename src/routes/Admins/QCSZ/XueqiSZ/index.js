import React, { Component, Fragment } from 'react';
import { Button, Dropdown, Icon, Menu, Tabs } from 'antd';
import style from './index.less';
import EditableTable from "../../../../components/EditableTable";
import { connect } from 'dva';

const { TabPane } = Tabs;
const onClick = ({ key }) => {

};

const menu = (
  <Menu onClick={onClick}>
    <Menu.Item key="1">1st menu item</Menu.Item>
    <Menu.Item key="2">2nd memu item</Menu.Item>
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);
@connect(({ XueqiSZ }) => ({ data: XueqiSZ.data }))
export default class XueqiSZ extends Component {
  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
  };

  columns = [{
    title: '学期编号',
    dataIndex: 'semeNo',
    width: '10%',
    render: (text, record) => this.renderColumns(text, record, 'semeNo'),
  }, {
    title: '学期名称',
    dataIndex: 'semeName',
    width: '10%',
    render: (text, record) => this.renderColumns(text, record, 'semeName'),
  }, {
    title: '学期类型',
    dataIndex: 'semeType',
    width: '10%',
    render: (text, record) => this.renderColumns(text, record, 'semeType'),
  }, {
    title: '学期开始时间',
    dataIndex: 'semeStart',
    render: (text, record) => this.renderColumns(text, record, 'semeStart'),
  }, {
    title: '学期结束时间',
    dataIndex: 'semeEnd',
    render: (text, record) => this.renderColumns(text, record, 'semeEnd'),
  },
  ];

  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  };

  load = () => {
    this.props.dispatch({
      type: 'XueqiSZ/load',
    });
  };

  constructor(props) {
    super(props);
    this.props.dispatch({
      type: 'XueqiSZ/load',
    });
  }

  save(key) {
    const newData = [...this.props.data];
    const target = newData.filter(item => key === item.key)[0];
    if (target) {
      delete target.editable;
      this.setState({ data: newData });
      this.cacheData = newData.map(item => ({ ...item }));
    }
  }

  render() {
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    if (!this.state.loaded) {
      this.load();
    }
    return (
      <Fragment>
        <Tabs type="card" className={style.xueQiSZInfo}>
          <TabPane tab="学期设置" key="1">
            <div className={style.xueQiSZ}>
              <div className={style.topSearch}>
                <div>
                  <span>单位：</span>
                  <Dropdown overlay={menu}>
                    <a className="ant-dropdown-link" href="#">
                      大连理工大学 <Icon type="down"/>
                    </a>
                  </Dropdown>
                </div>
                <div>
                  <span>学年：</span>
                  <Dropdown overlay={menu}>
                    <a className="ant-dropdown-link" href="#">
                      2017-2018 <Icon type="down"/>
                    </a>
                  </Dropdown>
                </div>
                <Icon type="search" className={style.iconSearch}/>
              </div>
              <div className={style.operation}>
                <Button htmlType={"button"}>增加</Button>
                <Button htmlType={"button"}>删除</Button>
                <Button htmlType={"button"}>刷新</Button>
                <Button htmlType={"button"}>保存</Button>
              </div>
              <div>
                <div style={{ marginBottom: 16 }}>
                  <span style={{ marginLeft: 8 }}> </span>
                </div>
                <EditableTable
                  dataSource={this.props.data}
                  columns={this.columns}
                  save={this.save.bind(this)}
                  rowSelection={rowSelection}
                />
              </div>
            </div>
          </TabPane>
          <TabPane tab="Tab 2" key="2">Content of Tab Pane 2</TabPane>
          <TabPane tab="Tab 3" key="3">Content of Tab Pane 3</TabPane>
        </Tabs>
      </Fragment>
    );
  }
}
