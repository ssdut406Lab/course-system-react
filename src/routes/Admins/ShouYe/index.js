import React, { Component, Fragment } from 'react';
import { Icon, Tabs } from 'antd';
import style from './index.less';

const { TabPane } = Tabs;

export default class PeiyangJH extends Component {
  render() {
    return (
      <Fragment>
        <Tabs type="card" className={style.peiyangJHInfo}>
          <TabPane tab="我的关注" key="1">
            <div>
              <div className={style.main_body}>
                {/*我的消息*/}
                <div className={style.my_message}>
                  <div className={style.message_class}>
                    <div className={style.inner_up_box}>
                      <span className={style.inner_up_box_text}>我的消息</span>
                      <div className={style.inner_up_box_icon}><Icon type="close-circle-o"/></div>
                    </div>
                    <div className={style.inner_down_box}>
                      <div className={style.main_text}>
                        <Icon type="right"/>
                        <span>我的消息1</span>
                      </div>
                      <div className={style.main_text}>
                        <Icon type="right"/>
                        <span>我的消息2</span>
                      </div>
                      <div className={style.main_text}>
                        <Icon type="right"/>
                        <span>我的消息3</span>
                      </div>
                    </div>
                  </div>
                </div>
                {/*我的审批*/}
                <div className={style.my_shenpi}>
                  <div className={style.message_class}>
                    <div className={style.inner_up_box}>
                      <span className={style.inner_up_box_text}>我的审批</span>
                      <div className={style.inner_up_box_icon}><Icon type="close-circle-o"/></div>
                    </div>
                    <div className={style.inner_down_box}>
                      <div className={style.main_text}>
                        <Icon type="right"/>
                        <span>我的审批1</span>
                      </div>
                      <div className={style.main_text}>
                        <Icon type="right"/>
                        <span>我的审批2</span>
                      </div>
                      <div className={style.main_text}>
                        <Icon type="right"/>
                        <span>我的审批3</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className={style.main_body}>
                {/*系统通知*/}
                <div className={style.sys_tongzhi}>
                  <div className={style.message_class}>
                    <div className={style.inner_up_box}>
                      <span className={style.inner_up_box_text}>系统通知</span>
                      <div className={style.inner_up_box_icon}><Icon type="close-circle-o"/></div>
                    </div>
                    <div className={style.inner_down_box}>
                      <div className={style.main_text}>
                        <Icon type="right"/>
                        <span>系统通知1</span>
                      </div>
                      <div className={style.main_text}>
                        <Icon type="right"/>
                        <span>系统通知2</span>
                      </div>
                      <div className={style.main_text}>
                        <Icon type="right"/>
                        <span>系统通知3</span>
                      </div>
                    </div>
                  </div>
                </div>
                {/*系统公告*/}
                <div className={style.sys_gonggao}>
                  <div className={style.message_class}>
                    <div className={style.inner_up_box}>
                      <span className={style.inner_up_box_text}>系统公告</span>
                      <div className={style.inner_up_box_icon}><Icon type="close-circle-o"/></div>
                    </div>
                    <div className={style.inner_down_box}>
                      <div className={style.main_text}>
                        <Icon type="right"/>
                        <span>系统公告1</span>
                      </div>
                      <div className={style.main_text}>
                        <Icon type="right"/>
                        <span>系统公告2</span>
                      </div>
                      <div className={style.main_text}>
                        <Icon type="right"/>
                        <span>系统公告3</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </TabPane>
        </Tabs>
      </Fragment>
    );
  }
}
