import React, { Component, Fragment } from 'react';
import { Button, Card, Menu, Popover, Radio, Table } from 'antd';
import style from './index.less';

const { Group: RadioGroup } = Radio;

const onClick = ({ key }) => {

};

const columns = [{
  title: '评价学期',
  dataIndex: 'semester',
}, {
  title: '课程编号',
  dataIndex: 'class_id',
}, {
  title: '课程名称',
  dataIndex: 'class_name',
}, {
  title: '课程学分',
  dataIndex: 'score',
}, {
  title: '实验名/课序号',
  dataIndex: 'nameid',
}, {
  title: '教师姓名',
  dataIndex: 'teacher_name',
}];

const data = [{
  semester: '2017-2018秋',
  class_id: '20180101',
  class_name: "计算机组成原理",
  score: 2,
  nameid: '03',
  teacher_name: '杨**',
}];

const columns_pg = [{
  title: 'title',
  dataIndex: 'title',
}, {
  title: 'grade',
  dataIndex: 'grade',
}];

const data_pg = [{
  title: <span className={style.radio_type}>教学态度</span>,
  grade: <div className={style.radio_type}>
    <RadioGroup name="radiogroup" defaultValue={1}>
      <Radio value={1}>A</Radio>
      <Radio value={2}>B</Radio>
      <Radio value={3}>C</Radio>
      <Radio value={4}>D</Radio>
    </RadioGroup>
  </div>,
}, {
  title: <span className={style.radio_type}>教学内容</span>,
  grade: <div className={style.radio_type}>
    <RadioGroup name="radiogroup" defaultValue={1}>
      <Radio value={1}>A</Radio>
      <Radio value={2}>B</Radio>
      <Radio value={3}>C</Radio>
      <Radio value={4}>D</Radio>
    </RadioGroup>
  </div>,
}, {
  title: <span className={style.radio_type}>教学方法</span>,
  grade: <div className={style.radio_type}>
    <RadioGroup name="radiogroup" defaultValue={1}>
      <Radio value={1}>A</Radio>
      <Radio value={2}>B</Radio>
      <Radio value={3}>C</Radio>
      <Radio value={4}>D</Radio>
    </RadioGroup>
  </div>,
}, {
  title: <span className={style.radio_type}>教学效果</span>,
  grade: <div className={style.radio_type}>
    <RadioGroup name="radiogroup" defaultValue={1}>
      <Radio value={1}>A</Radio>
      <Radio value={2}>B</Radio>
      <Radio value={3}>C</Radio>
      <Radio value={4}>D</Radio>
    </RadioGroup>
  </div>,
}, {
  title: <span className={style.radio_type}>备注</span>,
  grade: <div className={style.radio_type}>
    <input placeholder="备注信息"/>
  </div>,
}, {
  title: <span className={style.radio_type}>'项目总体评价'</span>,
  grade: <div className={style.radio_type}>
    <RadioGroup name="radiogroup" defaultValue={1}>
      <Radio value={1}>A</Radio>
      <Radio value={2}>B</Radio>
      <Radio value={3}>C</Radio>
      <Radio value={4}>D</Radio>
    </RadioGroup>
  </div>,
}];

const menu = (
  <Menu onClick={onClick}>
    <Menu.Item key="1">1st menu item</Menu.Item>
    <Menu.Item key="2">2nd memu item</Menu.Item>
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);

const tabList = [{
  key: 'tab1',
  tab: '教学评价',
}, {
  key: 'tab2',
  // tab: 'tab2',
}];

const customDot = (dot, { status, index }) => (
  <Popover content={<span>step {index} status: {status}</span>}>
    {dot}
  </Popover>
);

const contentList = {
  tab1: <p>
    <div>
      <Table
        pagination={false}
        columns={columns}
        dataSource={data}
        bordered
      />
      <div className={style.biaozhun}>
        <span className={style.bztype}>内容评价标准（A=90分；B=80分；C=70分；D=60分）</span>
      </div>
      <Table
        pagination={false}
        columns={columns_pg}
        dataSource={data_pg}
        showHeader={false}
        bordered
      /><br/>
      <div className={style.btn_bottom}>
        <Button>提交</Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <Button>返回</Button>
      </div>
    </div>
  </p>,
  tab2: <p>content2</p>,
};

export default class PeiyangJH extends Component {
  state = {
    key: 'tab1',
    noTitleKey: 'article',
  };
  onTabChange = (key, type) => {
    console.log(key, type);
    this.setState({ [type]: key });
  };

  render() {
    return (
      <Fragment title="教学评价">
        <div>
          <Card
            style={{ width: '100%' }}
            title="教学管理"
            // extra={<a href="#">More</a>}
            tabList={tabList}
            onTabChange={(key) => {
              this.onTabChange(key, 'key');
            }}
          >
            {contentList[this.state.key]}
          </Card>
          <br/><br/>
        </div>
      </Fragment>
    );
  }
}
