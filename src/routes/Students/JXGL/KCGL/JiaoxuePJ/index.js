import React, { Component, Fragment } from 'react';
import { Card, Icon, Menu, Popover, Table } from 'antd';
import style from './index.less';

const onClick = ({ key }) => {

};

const columns = [{
  title: '评价学期',
  dataIndex: 'semester',
}, {
  title: '课程编号',
  dataIndex: 'class_id',
}, {
  title: '课程名称',
  dataIndex: 'class_name',
}, {
  title: '课程学分',
  dataIndex: 'score',
}, {
  title: '实验名/课序号',
  dataIndex: 'nameid',
}, {
  title: '教师姓名',
  dataIndex: 'teacher_name',
}, {
  title: '是否已评价',
  dataIndex: 'isPJ',
}, {
  title: '参加评价',
  dataIndex: 'doPJ',
}];

const data = [{
  semester: '2017-2018秋',
  class_id: '20180101',
  class_name: "计算机组成原理",
  score: 2,
  nameid: '03',
  teacher_name: '杨**',
  isPJ: "未参加",
  doPJ: <div className={style.doPJ}><Icon type="form"/></div>,
}, {
  semester: '2017-2018秋',
  class_id: '20180101',
  class_name: "大学物理",
  score: 2,
  nameid: '分光计',
  teacher_name: '李**',
  isPJ: "未参加",
  doPJ: <div className={style.doPJ}><Icon type="form"/></div>,
}, {
  semester: '2017-2018秋',
  class_id: '20180101',
  class_name: "大学物理",
  score: 2,
  nameid: '光电实验',
  teacher_name: '刘**',
  isPJ: "未参加",
  doPJ: <div className={style.doPJ}><Icon type="form"/></div>,
}];

const menu = (
  <Menu onClick={onClick}>
    <Menu.Item key="1">1st menu item</Menu.Item>
    <Menu.Item key="2">2nd memu item</Menu.Item>
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);

const tabList = [{
  key: 'tab1',
  tab: '教学评价',
}, {
  key: 'tab2',
  // tab: 'tab2',
}];

const customDot = (dot, { status, index }) => (
  <Popover content={<span>step {index} status: {status}</span>}>
    {dot}
  </Popover>
);

const contentList = {
  tab1: <p>
    <div>
      <Table
        columns={columns}
        dataSource={data}
        bordered
      />
    </div>
  </p>,
  tab2: <p>content2</p>,
};

export default class PeiyangJH extends Component {
  state = {
    key: 'tab1',
    noTitleKey: 'article',
  };
  onTabChange = (key, type) => {
    console.log(key, type);
    this.setState({ [type]: key });
  };

  render() {
    return (
      <Fragment title="教学评价">
        <div>
          <Card
            style={{ width: '100%' }}
            title="教学管理"
            // extra={<a href="#">More</a>}
            tabList={tabList}
            onTabChange={(key) => {
              this.onTabChange(key, 'key');
            }}
          >
            {contentList[this.state.key]}
          </Card>
          <br/><br/>
        </div>
      </Fragment>
    );
  }
}
