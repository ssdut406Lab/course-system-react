import React, { Component, Fragment } from 'react';
import { Button, Card, Dropdown, Icon, Input, Menu, Popover, Steps } from 'antd';
import style from './index.less';

const { Step } = Steps;
const { Group: ButtonGroup } = Button;
const { TextArea } = Input;
const onClick = ({ key }) => {

};

const menu = (
  <Menu onClick={onClick}>
    <Menu.Item key="1">1st menu item</Menu.Item>
    <Menu.Item key="2">2nd memu item</Menu.Item>
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);

const tabList = [{
  key: 'tab1',
  tab: '课后作业',
}, {
  key: 'tab2',
  // tab: 'tab2',
}];

const customDot = (dot, { status, index }) => (
  <Popover content={<span>step {index} status: {status}</span>}>
    {dot}
  </Popover>
);

const contentList = {
  tab1: <p>
    <div className={style.peiyangJH}>
      <div className={style.topSearch}>
        <div>
          <span>课程：</span>
          <Dropdown overlay={menu}>
            <a className="ant-dropdown-link" href="#">
              大学物理 <Icon type="down"/>
            </a>
          </Dropdown>
        </div>
        <div>
          <Dropdown overlay={menu}>
            <a className="ant-dropdown-link" href="#">
              分光计 <Icon type="down"/>
            </a>
          </Dropdown>
        </div>
      </div>
      <div className={style.operation}>
        <div className={style.upper_box}>
          <span>总成绩:</span>
          <div className={style.bottom_box}/>
          <div className={style.pleft}>作业状态:</div>
          <div className={style.bottom_box}/>
        </div>
      </div>
    </div>

    <div className={style.now_jindu}>
      <span className={style.dangqian}>当前进度：</span>
      <div className={style.jindu_tiao}>
        <Steps current={1} progressDot={customDot} size="default">
          <Step title="Waiting" description=""/>
          <Step title="In Progress" description=""/>
          <Step title="Still in Progress" description=""/>
          <Step title="Finished" description=""/>
        </Steps>
      </div>
    </div>
    <div className={style.timu}>
      <span>客观题</span>
      <div className={style.content}>
        1. 题目要求 （）<br/>
        A ______________<br/>
        B ______________<br/>
        C ______________<br/>
        D ______________<br/>
      </div>
    </div>
    <div className={style.my_answer}>
      我的答案是（B）
      <div className={style.btn}>
        <ButtonGroup>
          <Button htmlType="button">上一题</Button>
          <Button htmlType="button">下一题</Button>
          <Button htmlType="button">交卷</Button>
          <Button htmlType="button">放弃</Button>
        </ButtonGroup>
      </div>
    </div>
    <div className={style.line}/>
    <div className={style.timu}>
      <span>主观题</span>
      <div className={style.content}>
        2. 题目要求<br/>
      </div>
    </div>
    <div className={style.zhuguan_my_answer}>
      <TextArea placeholder="答题区" autosize={{ minRows: 2, maxRows: 6 }}/>
      <div className={style.zhuguan_btn}>
        <ButtonGroup>
          <Button>交卷</Button>
        </ButtonGroup>
      </div>
    </div>
  </p>,
  tab2: <p>content2</p>,
};

export default class PeiyangJH extends Component {
  state = {
    key: 'tab1',
    noTitleKey: 'article',
  };
  onTabChange = (key, type) => {
    console.log(key, type);
    this.setState({ [type]: key });
  };

  render() {
    return (
      <Fragment>
        <div>
          <Card
            style={{ width: '100%' }}
            title="作业管理"
            // extra={<a href="#">More</a>}
            tabList={tabList}
            onTabChange={(key) => {
              this.onTabChange(key, 'key');
            }}
          >
            {contentList[this.state.key]}
          </Card>
          <br/><br/>
        </div>
      </Fragment>
    );
  }
}
