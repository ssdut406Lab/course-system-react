import React, { Component } from 'react';
import { Divider, Table } from 'antd';


const columns = [{
  title: '学期',
  dataIndex: 'seme',
}, {
  title: '课程编号',
  dataIndex: 'semeNo',
}, {
  title: '课程名称',
  dataIndex: 'courseName',
}, {
  title: '       ',
  dataIndex: 'null',
}, {
  title: '学分',
  dataIndex: 'credit',
}, {
  title: '操作',
  key: 'action',
  render: (text, record) => (
    <span>
      <a href="#">编辑</a>
      <Divider type="vertical"/>
      <a href="#">删除</a>
      <Divider type="vertical"/>
      <a href="#" className="ant-dropdown-link">详情</a>
    </span>
  ),
}];

const data = [];
for (let i = 0; i < 120; i++) {
  data.push({
    key: i,
    seme: `第${i}学期`,
    semeNo: `1111${i}`,
    courseName: `${i}课`,
    null: '',
    credit: `${i}`,
  });
}


export default class PeiyangJHTable extends Component {
  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
  };

  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  };

  render() {
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    return (
      <div>
        <div style={{ marginBottom: 16 }}>
          <span style={{ marginLeft: 8 }}> </span>
        </div>
        <Table rowSelection={rowSelection} columns={columns} dataSource={data} pagination={{ showSizeChanger: true }}/>
      </div>
    );
  }
}
