export const query = (url) => {
  const headers = new Headers({
    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
  });

  const request = new Request(url, {
    method: 'POST',
    headers: headers,
    credentials: 'include',
  });

  return fetch(request)
    .then((res) => res.json())
    .catch(() => ({
      status: false,
      msg: '马天荟来啦！！',
    }));
};

export const request = (url, data) => {
  const headers = new Headers({
    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
  });

  const request = new Request(url, {
    method: 'POST',
    headers: headers,
    body: data,
    credentials: 'include',
  });

  return fetch(request)
    .then((res) => res.json())
    .catch(() => ({
      status: false,
      msg: '马天荟来啦！！',
    }));
};
