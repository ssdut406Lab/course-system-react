// use localStorage to store the authority info, which might be sent from server in actual project.
export const getAuthority = () => {
  return localStorage.getItem('antd-pro-authority') || 'admin';
};

export const setAuthority = (authority) => {
  return localStorage.setItem('antd-pro-authority', authority);
};
